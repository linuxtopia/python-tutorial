# while loop
counter = 0
print("#### While Loop ####")
while (counter <= 10):
    print(counter)
    counter = counter + 1

# for loop
print("#### For Loop ####")
for i in range(0, 10):
    print(i)

print("#### Print from our list ####")
list = [4, 5, 6, 7]
for elment in list:
    print(elment)
# continue & break
print("#### continue & break ####")
list2 = [4, 5, 6, 7]
for element in list:
    if (element == 5):
        #continue
        break
    print(element)

# Example
print("#### Example ####")
myList = [4, 6, 7, 2, 4, 6, 7]
s = 0
for el in myList:
    s = s + el  # s = 4 + 6 --> 10
    if (s > 10):
        break
    print(s)
# When can I use while & for loop?
