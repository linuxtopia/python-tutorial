#Read a file
#file = open("/home/manjaro/python-tutorial/readAndWriteFiles/myTest2.txt", "r")
#for line in file:
#    print(line.strip())

#Write in a file
#Students = ["Ahmed", "Mohamed", "Mina", "Khaled", "Esraa"]
#file = open("/home/manjaro/python-tutorial/readAndWriteFiles/myTest2.txt", "a")
#for names in Students:
#    file.write(names + "\n")
#file.close

#with!
#with open("/home/manjaro/python-tutorial/readAndWriteFiles/myTest2.txt", "r") as file:
#    for line in file:
#        print(line.strip())
