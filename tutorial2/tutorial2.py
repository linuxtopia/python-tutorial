# Lists in Python

myFriends = ["Ahmed", "Mina", "Mohamed", "Esraa"]
print(myFriends)

# Add a new Element in my list
myFriends.append("Khaled")
print(myFriends)

# Length of my list
print(len(myFriends))

print(myFriends[0])

# Delete last Element from my list
khaled = myFriends.pop()
print(khaled)
print(myFriends)



