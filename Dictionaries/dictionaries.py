# Dictionaries in Python
myList = {"Berlin" : "BER" , "Munich" : "MUC", "Cairo" : "CAI"}
print(myList)



# Add a new key and value in myList
myList["Frankfurt"] = "FRA"
print(myList)

#Delete from myList
del myList["Berlin"]

if ("Berlin") in myList:
    print("Yes")
else:
    print("No")

print(myList)

# get information 
#print(myList["Berlin"])

#print(myList.get("Berlin"))