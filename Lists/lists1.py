students = ["Ahmed", "Khaled", "Esraa", "Mina", "Hassan"] + ["Samir"]
#last_student = students.pop()
#print(last_student)
print(students)

#del students[5]
students.remove("Samir")
print(students)

print("################# List Slicing ##########################")
students = ["Ahmed", "Khaled", "Esraa", "Mina", "Hassan", "Samir"]
print(students[-1])
print(students[-2])

print(students[2:4]) # 4 - 2 = 2
print(students[0:3]) # 3 - 0 = 3
print("################# List Slicing with strings ##########################")

print("Hello World"[0:5])






