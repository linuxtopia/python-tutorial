# Convert Data

# String -> Integer
a = "5"
b = "6"
print(int(a) + int(b))

# String -> Float
num1 = "5.2"
num2 = "6.3"
print(float(num1) + float(num2))

# Integer -> String
age = 26
print("I am " + str(age) + " years old")

# Lists
myFriends = ["Ahmed", "Mina", "Mohamed", "Esraa"]
print(myFriends)
print(", ".join(myFriends))

# String -> List
names = "Ahmed, Mina, Mohamed, Esraa"
print(names.split(", "))


P = "I am 26 years old"
print(P.split(" "))
print(len(P.split(" ")))
