# Example:
# Write a programm that asks for a person's height and weight and then
# calculates and outputs the BMI based on this data
# BMI = [weight in kg] / [height in m]^2

print("BMI - Calculator")
weight_str = input("Please give me your weight: ")
height_str = input("Please give me your height: ")

weight = float(weight_str)
height = float(height_str)
BMI = weight / height **2

print("Your BMI is: " + str(round(BMI, 1)))

