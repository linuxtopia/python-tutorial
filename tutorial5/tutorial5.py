# in operator
Students = ["Ahmed", "Mohamed", "Mina", "Esraa"]
# Start if
if ("Khaled" in Students):
    print("Yes")
else:
    print("No")
# End if

Name = "Can you see me?"
# Start if
if ("?" in Name):
    print("Yes")
else:
    print("No")
# End if

# not operator
age = 26
# != equal not
# not True = False
# not False = True
# Start if
if (not age < 26):
    print("Age is not equal 26")
else:
    print("Age is equal 26")
# End if

# Start if
if ("Khaled" not in Students):
    print("Khaled is not in my list")
else:
    print("Khaled is in my list")
# End if

# elif
# ¥, $, ฿, €

Currency = "$"
if (Currency == "€"):
    print("Your Currency is €")
elif (Currency == "$"):
    print("Your Currency is $")
else:
    print("No")