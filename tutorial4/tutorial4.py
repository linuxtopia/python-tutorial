

# if else
a = 20
if (a < 30):
    print("The Variable of a is smaller than 30")
# End if

#Start if
if (a < 10):
    print("a is greater than 10")
else:
    print("a is smaller than 10")
#End if

word = "Hello"
# Start if
if (word == "Hello"):
    print("Word is Hello")
else:
    print("Word is not Hello")
# End if

# Start if
if (a == 20):
    a = 21
    print(a)
# End if

# < , > , <= , >= , == , !=
# Start if
if (a != 20):
    print("a is not 20")
# End if

# AND & OR Operators with if else

num = 15
# Start if
if (num < 10 and num == 15):
    print("num is 15")
else:
    print("I am here")
# End if

# Start if
if (num == 15 or num < 10):
    print("num is 15")
# End if

print("############ AND ##############")
print(True and True)
print(True and False)
print(False and True)
print(False and False)

print("############ OR ##############")
print(True or True)
print(True or False)
print(False or True)
print(False or False)
