# List
t = [1, 2, 3, 4]
print(t)
t.append(5)
print(t)

# Tuple
b = (1, 2, 3, 4)
print(b)

# pack and unpack

students = ("Ahmed Mohamed", 22)

name, age = students

#name = students[0]
#age = students[1]
print(name)
print(age)

st = [
    ("Ahmed Mohamed", 22),
    ("Khaled Eslam", 26)
]

for s in st:
    name, age = s
    print(name)
    print(age)








