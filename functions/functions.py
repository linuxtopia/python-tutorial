"""  
Why we need functions?
1- Clear code
2- We don't have to repeat the code block
3- you can call the code block from anywhere at any time 
"""

#function without parameters
def helloPrint():
    print("Hello world")
    print("Hello world")

#call helloPrint
helloPrint()


#function with one parameter
def myName(name):
    print(name)

#call myName
myName("Nader")


#function with parameters
def nameAndAge(name, age):
    print(name, age)

#call nameAndAge
nameAndAge("Nader", 27)


#call function from other function
def callAllFunctions():
    helloPrint()
    myName("Nader")
    nameAndAge("Nader", 27)


print("#### callAllFunctions ####")
#call callAllFunctions
callAllFunctions()


#function with return
def maximum(a, b):
    if (a < b):
        return b
    else:
        return a

result = maximum(5, 4)
print(result)